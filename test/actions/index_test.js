import { expect } from '../test_helper';
import { SAVE_COMMENT } from '../../src/actions/types';
import { saveComment } from '../../src/actions'

describe('action', () => {
    let action;

    beforeEach(() => {
        action = saveComment('new comment');
    });
    describe('Save comment', () => {
        it('has the correct type', () => {
            expect(action.type).to.equals(SAVE_COMMENT);
        });

        it('has the correct payload', () => {
            expect(action.payload).to.contain('new comment');
        });
    });
});